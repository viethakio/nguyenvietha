/*

Array of object

Top level object can be:
1. Header
2. Group (Group can have navItems as children)
3. navItem

* Supported Options

/--- Header ---/

header

/--- nav Grp ---/

title
icon (if it's on top level)
tag
tagVariant
children

/--- nav Item ---/

icon (if it's on top level)
title
route: [route_obj/route_name] (I have to resolve name somehow from the route obj)
tag
tagVariant

*/
import dashboard from './dashboard'
import products from './products'
import articles from './articles'
import charts from './charts'
import figmapage from './figmapage'
import vue3 from './vue3'
import demo from './demo'

// Array of sections
export default [...dashboard, ...products, ...articles, ...charts, ...figmapage, ...vue3, ...demo]
