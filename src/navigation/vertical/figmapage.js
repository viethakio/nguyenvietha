export default [
  {
    title: 'Figma',
    route: 'figma',
    icon: 'FigmaIcon',
  },
]
