export default [
  {
    title: 'Biểu đồ',
    icon: 'PieChartIcon',
    tagVariant: 'light-danger',
    children: [
      {
        title: 'Đầu mối',
        route: 'daumoi',
      },
      {
        title: 'Chuyên ngành',
        route: 'chuyennganh',
      },
    ],
  },
]
