export default [
  {
    path: '/daumoi',
    name: 'daumoi',
    component: () => import('@/views/DauMoi.vue'),
  },
  {
    path: '/chuyennganh',
    name: 'chuyennganh',
    component: () => import('@/views/ChuyenNganh.vue'),
  },
]
