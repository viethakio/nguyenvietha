export default [
  {
    path: '/articles',
    name: 'articles',
    component: () => import('@/views/Articles.vue'),
  },
]
