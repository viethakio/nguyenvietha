export default [
  {
    path: '/products',
    name: 'products',
    component: () => import('@/views/Products.vue'),
  },
]
