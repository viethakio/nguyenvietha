import { get } from '@/@fake-db/data/products/product'

const getListProduct = (context, payload) => {
  const itemsPerPage = payload && payload.pageSize ? payload.pageSize : 10
  const page = payload && payload.currentPage ? payload.currentPage : 1
  get({ filter: {}, limit: itemsPerPage, page }).then(response => {
    if (response && response.data.length > 0) {
      context.commit('GET_LIST_PRODUCT', response)
    }
  })
}

const removeProduct = (context, id) => {
  context.commit('REMOVE_PRODUCT', id)
}

export default {
  getListProduct,
  removeProduct,
}
