const GET_LIST_PRODUCT = (state, listProduct) => {
  state.listProduct = listProduct
}

const REMOVE_PRODUCT = (state, id) => {
  const { listProduct } = state
  listProduct.data = listProduct.data.filter(item => item.id !== id)
  state.listProduct = listProduct
}

export default {
  GET_LIST_PRODUCT,
  REMOVE_PRODUCT,
}
