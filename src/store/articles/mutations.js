const GET_LIST_ARTICLE = (state, listArticle) => {
  state.listArticle = listArticle
}

const REMOVE_ARTICLE = (state, id) => {
  state.listArticle = state.listArticle.filter(item => item.id !== id)
}

export default {
  GET_LIST_ARTICLE,
  REMOVE_ARTICLE,
}
