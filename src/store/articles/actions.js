import { get } from '@/@fake-db/data/articles/article'

const getListArticle = (context, payload) => {
  get(payload).then(response => {
    if (response && response.data.length > 0) {
      context.commit('GET_LIST_ARTICLE', response.data)
    }
  })
}

const removeArticle = (context, id) => {
  context.commit('REMOVE_ARTICLE', id)
}

export default {
  getListArticle,
  removeArticle,
}
