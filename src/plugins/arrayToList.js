export default {
  // eslint-disable-next-line no-unused-vars
  install(Vue, options) {
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$arrayToList = array => {
      if (array.length === 0) return '<ul></ul>'
      let html = ''
      html += '<ul>'
      for (let i = 0; i < array.length; i += 1) {
        html += `<li>${array[i]}</li>`
      }
      html += '</ul>'
      return html
    }
  },
}
