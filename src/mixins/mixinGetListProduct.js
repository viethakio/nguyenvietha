export default {
  async created() {
    this.$_mixinGetListProduct_getData(this.searchForm)
  },
  watch: {
    searchForm(newValue) {
      this.$_mixinGetListProduct_getData(newValue)
    },
    // currentPage() {
    //   this.$_mixinGetListProduct_getData(this.searchForm)
    // },
  },
  methods: {
    async $_mixinGetListProduct_getData(searchForm) {
      await this.$store.dispatch('products/getListProduct', searchForm)
    },
  },
}
