export default {
  async created() {
    const payload = { filter: {}, limit: 10, page: 1 }
    await this.$store.dispatch('articles/getListArticle', payload)
  },
}
