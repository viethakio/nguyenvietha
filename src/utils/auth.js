export const LOCAL_STORAGE_KEY = {
  ACCESS_TOKEN: 'accessToken',
  REFRESH_TOKEN: 'refreshToken',
  USER_DATA: 'userData',
}

export const getValueFromStorageByKey = key => JSON.parse(localStorage.getItem(key))

export const getValueUserFromStorageByKey = key => JSON.parse(localStorage.getItem(key))?.access_token

export const handleApi = (
  resolve,
  reject,
  context,
  mutationEvent,
  serviceFn,
) => {
  serviceFn
    .then(response => {
      if (response && response.data.length > 0) {
        context.commit(mutationEvent, response.data)
        resolve(response)
      } else {
        reject(response)
      }
    })
    .catch(error => {
      reject(error)
    })
}
