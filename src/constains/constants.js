// eslint-disable-next-line import/prefer-default-export
export const PRODUCT_TABLE_FIELDS = {
  img: 'Ảnh',
  name: 'Tên',
  price: 'Giá',
  categories: 'Loại',
  action: '',
}

export const ARTICLE_TABLE_FIELDS = {
  img: 'Ảnh',
  name: 'Tên',
  categories: 'Loại',
  text: 'Text',
  action: '',
}

export const CATEGORIES_DAU_MOI = [
  'Cty Microfsoft',
  'Cty Hachico',
  'Cty Ba Sao',
  'Cty Ngũ Sắc',
  'Cty Bảy Màu',
  'Cty Google',
  'Cty Facebook',
  'Cty IOS',
]

export const CATEGORIES_CHUYEN_NGANH = [
  'Trường Thịnh',
  'Tuệ Định',
  'Sao Khuê',
  'Việt Quang',
  'Bắc Đẩu',
  'Ban Mai',
  'Sao Xanh',
  'Hòa Phát',
]
